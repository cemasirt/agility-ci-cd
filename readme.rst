NGINX Plus CICD Lab
===================

Last Updated: **06/04/2020**
----------------------------

Instructors at Agility 2020:
----------------------------

-  Jason Williams
   <`ja.williams@f5.com <mailto:%3cja.williams@f5.com>`__>
-  Armand Sultantono <`armand@f5.com <mailto:%3carmand@f5.com>`__>
-  Doug Gallarda <d.gallarda@f5.com>
-  Chris Akker <c.akker@f5.com>

UDF Lab Maintainers:
--------------------

-  Armand Sultantono <`armand@f5.com <mailto:%3carmand@f5.com>`__>
-  Jason Williams
   <`ja.williams@f5.com <mailto:%3cja.williams@f5.com>`__>

Contents:
---------

-  `Introduction <0.Introduction/0.Introduction.md>`__
-  `Exercise 1.0. Creating Docker Images for Nginx
   Plus <1.0.creating-docker-images-for-nginx-plus/1.0.creating-docker-images-for-nginx-plus.md>`__

   -  Task 1: Run a pipeline to build NGINX Plus images
   -  Task 2: GitLab CI/CD environment variables
   -  Task 3: **Optional:** Pull and run a Docker image from our private
      Docker Registry

-  `Exercise 2.0. Deploying Nginx Plus Web Server with
   CICD <2.0.deploying-nginx-plus-web-server-with-cicd/2.0.deploying-nginx-plus-web-server-with-cicd.md>`__

   -  Task 1: Make new code commit, push changes and deploy to Staging
      and Production
   -  Task 2. **Optional:** Made new code commit, push changes using
      command line tools, and deploy

-  `Exercise 3.0. Continuous Deployment for NGINX Plus Load
   Balancers <3.0.continuous-deployment-for-nginx-plus-load-balancers/3.0.continuous-deployment-for-nginx-plus-load-balancers.md>`__

   -  Task 1: Make new code commit, push changes to a live running NGINX
      Plus Virtual Machine

--------------

Navigation
~~~~~~~~~~

   `Next - Introduction => <./0.Introduction/0.Introduction.md>`__
